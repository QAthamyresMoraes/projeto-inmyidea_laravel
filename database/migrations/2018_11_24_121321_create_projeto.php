<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjeto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projeto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_projeto');
            $table->string('descricao_projeto');
           # $table->foreign('id_investidor')->references('id')->on('investidor');
           # $table->foreign('id_empreendedor')->references('id')->on('empreendedor');
           # $table->foreign('perfil_usuario')->references('id')->on('perfil');    
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projeto');
    }
}
