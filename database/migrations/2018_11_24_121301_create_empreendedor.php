<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpreendedor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empreendedor', function (Blueprint $table) {
            $table->increments('id');
            $table->double('investimento_empreendedor', 8,2);
            $table->string('nome_empreendedor');
            $table->string('estado_civil_empreendedor');
            $table->string('ocupacao_empreendedor');
            $table->string('sexo_empreendedor');
            $table->string('documento_empreendedor');
            $table->string('telefone_empreendedor');
           # $table->foreign('id_projeto')->references('id')->on('projeto');
           # $table->foreign('id_dados_bancarios')->references('id')->on('dados_bancarios');
            #$table->foreign('id_endereco')->references('id')->on('endereco');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empreendedor');
    }
}
