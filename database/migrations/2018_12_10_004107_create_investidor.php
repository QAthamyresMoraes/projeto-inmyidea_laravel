<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestidor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investidor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_investidor');
            $table->string('documento_investidor');
            $table->string('ocupação_investidor');
            $table->string('estado_civil_investidor');
            $table->string('sexo_investidor');
            $table->string('telefone_investidor');
            $table->double('renda_investidor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investidor');
    }
}
