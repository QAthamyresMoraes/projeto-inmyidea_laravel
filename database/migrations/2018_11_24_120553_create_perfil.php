<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfil extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil', function (Blueprint $table) { 
            $table->increments('id');
            $table->string('perfil');
           # $table->foreing('id_empreendedor')->references('id')->on('empreendedor');
           # $table->foreing('id_administrador')->references('id')->on('administrador');
            #$table->foreing('id_investidor')->references('id')->on('investidor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfil');
    }
}
