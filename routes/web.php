<?php


Route::get('/',['as'=>'site.home', function(){
	return view('site.home');
}]);

Route::get('/sobre',['as'=>'site.sobre', function(){
	return view('site.sobre');
}]);

Route::get('/contato',['as'=>'site.contato', function(){
	return view('site.contato');
}]);



Route::get('/investidor/cadastro',['as'=>'investidor.cadastro', function(){
	return view('investidor.cadastro.index');
}]);


/*

Rota para consultar um projeto

*/

Route::get('/projeto/{id}/{titulo?}',['as'=>'site.projeto', function(){
	return view('site.projeto');
}]);



/*

Rota para sistema de administração

*/

Route::get('/admin/login', ['as'=>'admin.login', function(){
	return view('admin.login.index');
}]);

Route::post('/admin/login', ['as' =>'admin.login', 'uses' =>'Admin\UsuarioController@login']);


Route::group(['middleware'=>'guest'], function(){

	Route::get('/admin', ['as'=>'admin.principal', function(){
		return view('admin.principal.index');
	}]);

	Route::get('/admin/usuarios', ['as' =>'admin.usuarios', 'uses' =>'Admin\UsuarioController@index']);

	Route::get('/admin/login/sair', ['as' =>'admin.login.sair', 'uses' =>'Admin\UsuarioController@sair']);

	Route::get('/admin/usuarios/adicionar', ['as' =>'admin.usuarios.adicionar', 'uses' =>'Admin\UsuarioController@adicionar']);

	Route::get('/admin/usuarios/editar/{id}', ['as' =>'admin.usuarios.editar', 'uses' =>'Admin\UsuarioController@editar']);

	Route::post('/admin/usuarios/salvar', ['as' =>'admin.usuarios.salvar', 'uses' =>'Admin\UsuarioController@salvar']);

	Route::put('/admin/usuarios/atualizar/{id}', ['as' =>'admin.usuarios.atualizar', 'uses' =>'Admin\UsuarioController@atualizar']);

	Route::get('/admin/usuarios/deletar/{id}', ['as' =>'admin.usuarios.deletar', 'uses' =>'Admin\UsuarioController@deletar']);
});



/*

Rota para investidor - Controller

*/

Route::get('/login',['as'=>'site.login', function(){
	return view('site.login');
}]);

Route::post('/login', ['as' =>'site.login', 'uses' =>'Usuario\UsuarioController@login']);

Route::get('/usuarios/adicionar', ['as' =>'site.usuarios.adicionar', 'uses' =>'Usuario\UsuarioController@adicionar']);

Route::post('/usuarios/salvar', ['as' =>'site.usuarios.salvar', 'uses' =>'Usuario\UsuarioController@salvar']);




Route::get('/minhaArea',['as'=>'site.minhaArea', function(){
	return view('site.usuarios.index');
}]);
