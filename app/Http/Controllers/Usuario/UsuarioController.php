<?php

namespace App\Http\Controllers\Usuario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Investidor;

use App\User;

class UsuarioController extends Controller
{
	public function login(Request $request)
	{
		$dados = $request->all();

		if(Auth::attempt(['email'=>$dados['email'], 'password'=>$dados['password']])){
			\Session::flash('mensagem',['msg'=> 'Login realizado com sucesso!', 'class'=>'green white-text']);

			return redirect()->route('site.minhaArea');
		}

		\Session::flash('mensagem',['msg'=> 'Usuário ou senha incorretos!', 'class'=>'red white-text']);
		return redirect()->route('site.login');
	}


	public function adicionar()
	{
		return view('site.usuarios.adicionar');
	}

	public function salvar(Request $request)
	{
		$dados = $request->all();

		$investidor = new Investidor();
		$investidor->documento_investidor= $dados['documento_investidor'];
		$investidor->nome_investidor= $dados['nome_investidor'];
		$investidor->sexo_investidor= $dados['sexo_investidor'];
		$investidor->estado_civil_investidor= $dados['estado_civil_investidor'];
		$investidor->ocupação_investidor= $dados['ocupação_investidor'];
		$investidor->telefone_investidor= $dados['telefone_investidor'];
		$investidor->renda_investidor= $dados['renda_investidor'];

		$usuario = new User();
		$usuario->name= $dados['nome_investidor'];
		$usuario->email= $dados['email'];
		$usuario->password= bcrypt($dados['password']);


		$usuario->save();
		$investidor->save();

		\Session::flash('mensagem',['msg'=> 'Usuário com perfil INVESTIDOR cadastrado com sucesso!', 'class'=>'green white-text']);


		return redirect()->route('site.home');

	}
}
