<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class empreendedor extends Model
{
	protected $fillable = ['nome_empreendedor','estado_civil_empreendedor','ocupacao_empreendedor','sexo_empreendedor', 'documento_empreendedor', 'telefone_empreendedor'];
    protected $table = 'empreendedor';
}
