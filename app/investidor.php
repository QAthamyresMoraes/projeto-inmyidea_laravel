<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investidor extends Model
{
	protected $fillable = ['nome_investidor','documento_investidor','ocupação_investidor', 'estado_civil_investidor','sexo_investidor', 'telefone_investidor', 'renda_investidor'];
	protected $table = 'investidor';
}
