  $(document).ready(function(){
  	$('.sidenav').sidenav();

  	$('.slider').slider({full_wodth: true});
  });

  function sliderPrev(){
  	$('.slider').slider('pause');
  	$('.slider').slider('prev');
  }


  function sliderNext(){
  	$('.slider').slider('pause');	
  	$('.slider').slider('next');	
  }


  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('select').formSelect();
  });
  