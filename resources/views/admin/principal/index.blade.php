@extends('layouts.app')

@section('content')

<div class ="container">
	<h2>Área de administração inMyIdea</h2>
	
  <div class="row">
    <div class="col s12 m6">
      <div class="card">
        <div class="card-image">
          <img class="responsive-img" src="{{asset('img/addAdmin.png ')}}">
          <span class="card-title">Adicionar administrador</span>
          <a class="btn-floating halfway-fab waves-effect waves-light red" href="{{route('admin.usuarios')}}"><i class="material-icons">add</i></a>
        </div>
        <div class="card-content">
          <p>Se deseja adicionar um administrador no inMyIdea, é por aqui!</p>
        </div>
      </div>
    </div>
  </div>
           
	<div>

@endsection