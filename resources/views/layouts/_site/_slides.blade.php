<div class="slider">
	<ul class="slides">
		<li>
			<img src="{{asset('img/slide1.jpg')}}" alt="parceiros">
			<div class="caption left-align">
				<h3 class="grey-text text-darken-4" >Está precisando daquela força?</h3>
				<h5 class="grey-text text-darken-2" >No InMyIdea pode ter um investidor querendo te ajudar!</h5>
			</div>
		</li>
	</ul>
</div>