
<div class="row section">
	<h3 align="center">O que é o inMyIdea?</h3>
	<p align="center">O projeto In My Idea é uma aplicação web que oferece uma vitrine de projetos esperando por dois perfis de investidores: Investidor Anjo e Crowdfunding (Investimento Coletivo). Empreenderes publicam as suas ideias e escolhem o perfil de investidor que querem associados a seus projetos, assim os investidores analisam as condições propostas pelos empreendedores, entram em negociação imediata ou enviam uma contraproposta. </p>
	<div class="divider"></div>
	<h3 align="center" >Faça parte do inMyIdea!</h3>
</div>	
<div class="col s12 m7">
	<div class="card horizontal">
		<div class="card-image">
			<img class="responsive-img" src="img/emp.png">
		</div>
		<div class="card-stacked">
			<div class="card-content">
				<p>Se você tem um ideia e acredita que com um bom investimento pode dar certo, então publique-a no inMyIdea
				e tenha a chance de tocar o seu negócio com a ajuda de investidores.</p>
			</div>
			<div class="card-action">
				<a href="/nalista">EU TENHO UMA IDEIA!</a>
			</div>
		</div>
	</div>
</div>

	<div class="row section">
		<div class="col s12 m6">
			<div class="responsive-img" class="card">
				<div class="card-image">
					<img class="responsive-img" src="img/investidorAnjo.jpg">
				</div>
				<div class="card-content">
					<p><b class="deep-orange-text darken-1">Investidor Anjo</b></p>
					<p>Eles se tornam sócios investidores do seu negócio e, muitas vezes, grandes mentores. Especializam-se em apoiar projetos em fase inicial e, por entenderem do mercado alvo, procuram empresas que, para eles, possuem capacidade de seguir a tendência crescente. 
					</p>
					<br>
					<br>
					<a class="btn teal lighten-1" href="/usuarios/adicionar">Quero ser investidor!</a>
				</div>
			</div>
		</div>

		<div class="col s12 m6">
			<div class="responsive-img" class="card">
				<div class="card-image">
					<img class="responsive-img" src="img/CF.jpg">
				</div>
				<div class="card-content">
					<p><b class="deep-orange-text darken-1">Crowdfunding (INVESTIMENTO COLETIVO)</b></p>
					<p>O objetivo é reunir diversas pessoas que possam colaborar com pequenas quantias e, assim, viabilizar a ideia. Recebendo, ou não, uma contrapartida por isto. Essa é uma forma rápida e relativamente simples de captar valores para a execução de uma ideia ou projeto de apelo popular, com uma baixa contrapartida.
					</p>
					<br>
					<a class="btn teal lighten-1" href="/usuarios/adicionar">Quero ser investidor!</a>
				</div>
			</div>
		</div>

</div>