@extends('layouts.site')

@section('content')
<div class ="container">
	<h2 align="center">CADASTRO INVESTIDOR</h2>
	<form action="#" method="post">
		{{csrf_field()}}
		@include('investidor.cadastro._form')
		<button class="btn cyan darken-4 ">Cadastrar</button>

	</form>
	<br>
	<div>
@endsection