@extends('layouts.site')

@section('content')
@include('layouts._site._slides')

{{ csrf_field() }}
<div class="container">
   @include('layouts._site._tipo_investidor')
</div>
@endsection
