@extends('layouts.site')

@section('content')

<div class ="container">
	<h2 align="center">CADASTRO INVESTIDOR</h2>

	<form action="{{ route(	'site.usuarios.salvar' )}} " method="post">

		{{ csrf_field() }}

		@include('site.usuarios._formInvestidor')
		
		<button class="btn cyan darken-4 ">Cadastrar</button>

	</form>
	<br>
	<div>

@endsection
