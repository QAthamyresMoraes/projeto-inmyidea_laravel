<br>
<div class="row">
	<h4>Tipo de Investidor</h4>

	<div>
		<label class="col s12 m12">
			<input type="checkbox" name="tipo_investidor" value="investidorAnjo" class="filled-in" checked="checked" />
			<span>Investidor Anjo</span>
		</label>
	</div>
	<div>
		<label class="col s12 m12">
			<input type="checkbox" name="tipo_investidor" value="crowdfunding" class="filled-in" checked="checked" />
			<span>Crowdfunding (INVESTIMENTO COLETIVO)</span>
		</label>
	</div>

	<div col s12 m12">
		<h4>Dados Básicos</h4>
	</div>
	<div class="input-field col s4 m4">
		<input id="documento_investidor" name="documento_investidor" type="text" class="validate">
		<label >CPF/CNPJ</label>
	</div>

	<div class="input-field col s12 m12">
		<input id="nome_investidor" name="nome_investidor" type="text" class="validate">
		<label for="first_name">Nome completo</label>
	</div>

	<div class="input-field col s6 m6">
		<input id="sexo_investidor" name="sexo_investidor" type="text">
		<label for="sexo_investidor">Sexo</label>
	</div>

	
	<div class="input-field col s6 m6">
		<input id="estado_civil_investidor" name="estado_civil_investidor" type="text">
		<label for="estado_civil_investidor">Estado Civil</label>
	</div>

	<div class="input-field col s6 m6">
		<input id="ocupação_investidor" name="ocupação_investidor" type="text" class="validate">
		<label for="ocupação_investidor">Ocupação</label>
	</div>

	<div class="input-field col s6 m6">
		<input id="renda_investidor" name="renda_investidor" type="number" class="validate">
		<label for="renda_investidor">Renda</label>
	</div>

	<div class="input-field col s12 m12">
		<h4>Dados Bancários</h4>
	</div>
	<div class="input-field col s2 m2">
		<input id="banco" name="banco" type="text" class="validate">
		<label for="banco">Banco</label>
	</div>

	<div class="input-field col s2 m2">
		<input id="agencia" name="agencia" type="text" class="validate">
		<label for="agencia">Agência</label>
	</div>

	<div class="input-field col s2 m2">
		<input id="conta" name="conta" type="number" class="validate">
		<label for="conta">Conta</label>
	</div>

	<div class="input-field col s12 m12">
		<h4>Contato</h4>
	</div>
	<div class="input-field col s6 m6">
		<input id="telefone_investidor" name="telefone_investidor" type="text" class="validate">
		<label for="telefone_investidor">Telefone</label>
	</div>


	<div class="input-field col s12 m12">
		<h4>Endereço</h4>
	</div>
	<div class="input-field col s6 m6">
		<input id="cep" name="" type="text" class="validate">
		<label for="cep">CEP</label>

	</div>
	<div class="input-field col s8 m8">
		<input id="rua" name="rua" type="text" class="validate">
		<label for="rua">Rua</label>

	</div>
	<div class="input-field col s2 m2">
		<input id="numero" name="numero" type="number" class="validate">
		<label for="numero">Nº</label>

	</div>
	<div class="input-field col s4 m4">
		<input id="bairro" name="bairro" type="text" class="validate">
		<label for="bairro">Bairro</label>
		<br>
	</div>
	<div class="input-field col s4 m4">
		<input id="estado" name="estado" type="text" class="validate">
		<label for="municipio">Estado</label>
	</div>

	<div class="input-field col s4 m4">
		<input id="municipio" name="municipio" type="text" class="validate">
		<label for="municipio">Municipio</label>
	</div>
	<div class="input-field col s12 m12">
		<h4>Login</h4>
	</div>
	<div class="input-field col s8 m8">
		<input type="text" name="email" class="validate">
		<label>E-mail</label>

	</div>
	<div class="input-field col s8 m8">
		<input type="password" name="password" class="validate">
		<label>Senha</label>

	</div>

</div>
