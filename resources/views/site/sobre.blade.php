@extends('layouts.site')

@section('content')

<div class="container">
	<div class="row section">
		<h3 align="center">Sobre</h3>
		<div class="divider"></div>
	</div>
	<div class="row section">
		<div class="col s12 m8">

			<img class="responsive-img" src="{{asset('img/logo.png')}}">
		</div>
		<div class="col s12 m4">
			<h4>O inMyIdea</h4>
			<blockquote>
				Descrição breve sobre o inMyIdea.
			</blockquote>
			<p>Sobre a empresa</p>
		</div>
	</div>
</div>
@endsection
