@extends('layouts.site')

@section('content')

<div class ="container">
	<h2>Entrar</h2>
	<form action="{{ route('site.login')}}" method="post">
		{{ csrf_field() }}
		@include('site.usuarios._form')
		
	<button class="btn cyan darken-4">Entrar</button>

</form>
<div>
@endsection