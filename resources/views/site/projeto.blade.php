@extends('layouts.site')

@section('content')

<div class="container">
	<div class="row section">
		<h3 align="center">Projeto</h3>
		<div class="divider"></div>
	</div>
	<div class="row section">
		<div class="col s12 m8">
			<div class="row">
				<div class="slider">
					<ul class="slides">
						<li>
							<img src="{{asset('img/projeto1.png')}}">
							<div class="caption left-align">
								<h3 class="grey-text text-darken-4" >Está precisando daquela força?</h3>
								<h5 class="grey-text text-darken-2" >No InMyIdea pode ter um investidor querendo te ajudar!</h5>
							</div>
						</li>
						<li>
							<img src="{{asset('img/parceiros.png')}}">
							<div class="caption left-align">
								<h3 class="grey-text text-darken-4" >Está precisando daquela força?</h3>
								<h5 class="grey-text text-darken-2" >No InMyIdea pode ter um investidor querendo te ajudar!</h5>
							</div>
						</li>

					</ul>
				</div>
			</div>
			<div class="row" align="center">
				<button onclick="sliderPrev()"class="btn blue">Anterior</button>
				<button onclick="sliderNext()"class="btn blue">Próxima</button>
			</div>
		</div>
		<div class="col s12 m4">
			<h4>Titulo do projeto</h4>
			<blockquote>
				Descrição do projeto.
			</blockquote>
			<p><b>Código do projeto: </b>1</p>
			<p><b>Status: </b>Aguardando</p>
			<p><b>Tipo de investidor: </b> Anjo / Crowdfunding</p>
			<a class="btn deep-orange darken-1" href="{{}}">Quero fazer uma proposta!</a>
		</div>
	</div>
</div>
@endsection
